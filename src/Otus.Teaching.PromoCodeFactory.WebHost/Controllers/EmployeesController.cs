﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Сотрудники
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class EmployeesController
        : ControllerBase
    {
        private readonly IRepository<Employee> _employeeRepository;

        public EmployeesController(IRepository<Employee> employeeRepository)
        {
            _employeeRepository = employeeRepository;
        }

        /// <summary>
        /// Получить данные всех сотрудников
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<List<EmployeeShortResponse>> GetEmployeesAsync()
        {
            var employees = await _employeeRepository.GetAllAsync();

            var employeesModelList = employees.Select(x =>
                new EmployeeShortResponse()
                {
                    Id = x.Id,
                    Email = x.Email,
                    FullName = x.FullName,
                }).ToList();

            return employeesModelList;
        }

        /// <summary>
        /// Получить данные сотрудника по Id
        /// </summary>
        /// <returns></returns>
        [HttpGet("{id:guid}")]
        public async Task<ActionResult<EmployeeResponse>> GetEmployeeByIdAsync(Guid id)
        {
            var employee = await _employeeRepository.GetByIdAsync(id);

            if (employee == null)
                return NotFound();

            var employeeModel = new EmployeeResponse()
            {
                Id = employee.Id,
                Email = employee.Email,
                Roles = employee.Roles.Select(x => new RoleItemResponse()
                {
                    Name = x.Name,
                    Description = x.Description
                }).ToList(),
                FullName = employee.FullName,
                AppliedPromocodesCount = employee.AppliedPromocodesCount
            };

            return employeeModel;
        }

        /// <summary>
        /// Создать данные сотрудника.
        /// </summary>
        /// <returns></returns>
        [HttpPost("Create/{model}")]
        public void CreateEmployee(EmployeeResponse model)
        {
            var nameArray = model.FullName.Split(' ');
            var obj = new Employee()
            {
                Id = Guid.NewGuid(),
                Email = model.Email,
                FirstName = nameArray[0],
                LastName = nameArray[1],
                AppliedPromocodesCount = model.AppliedPromocodesCount,
                Roles = model.Roles.Select(x => new Role() { Id = x.Id, Name = x.Name, Description = x.Description }).ToList()
            };

            _employeeRepository.Create(obj);
        }

        /// <summary>
        /// Изменить данные сотрудника.
        /// </summary>
        /// <returns></returns>
        [HttpPut("Update/{model}")]
        public void UpdateEmployee(EmployeeResponse model)
        {
            var nameArray = model.FullName.Split(' ');
            var obj = new Employee()
            {
                Id = model.Id,
                Email = model.Email,
                FirstName = nameArray[0],
                LastName = nameArray[1],
                AppliedPromocodesCount = model.AppliedPromocodesCount,
                Roles = model.Roles.Select(x => new Role() { Id = x.Id, Name = x.Name, Description = x.Description }).ToList()
            };

            _employeeRepository.Update(obj);
        }

        /// <summary>
        /// Удалить данные сотрудника.
        /// </summary>
        /// <returns></returns>
        [HttpDelete("Delete/{id:guid}")]
        public void DeleteEmployee(Guid id)
        {
            _employeeRepository.Delete(id);
        }
    }
}